'use strict';

module.exports = function ($, BGVars, BGPaths, folder) {
	$.gulp.task('js:' + folder, function(){
		return $.gulp.src( [BGPaths.src[folder].js + '/**/*.js' , '!' + BGPaths.src[folder].js + '/_**/*.js'])
			.pipe($.sourcemaps.init())
			.pipe($.include())
			/*.pipe($.babel({
				presets: ['babel-preset-es2015']
			}))
				.on('error', $.functions.onError)*/
			.pipe($.jshint())
			.pipe($.gulpif( 'prod' == BGVars.stage, $.uglify()) )
			.pipe($.sourcemaps.write('../maps'))
			.pipe($.gulp.dest( BGPaths.build[folder].js ))
			.pipe($.browserSync.stream());
	});
};