'use strict';

module.exports = function ($, BGVars, BGPaths, BGTasks, project) {
	/**
	 * Browser Sync
	 */

	$.gulp.task('browser-sync', function() {

		let broArgs = {
			serveStatic     : ['.', './bower_components'],
			host            : project.local.host,
			open            : false,
			ghostMode       : false,
			logConnections  : true,
			reloadOnRestart : true,
			reloadDebounce  : 2000,
			notify          : {
				styles : {
					top                    : 'auto',
					bottom                 : '5px',
					right                  : '5px',
					borderBottomLeftRadius : '0',
				}
			}
		};

		if( project.local.proxy ) {
			broArgs.proxy = project.local.proxy;
		}
		else {
			broArgs.server = { baseDir : [] };
			
			for (let folder in BGPaths.build){
				broArgs.server.baseDir.push(BGPaths.build[folder].files);
			}
		}

		$.browserSync.init(broArgs);


	});

	/**
	 * Theme Watch
	 */
	$.gulp.task('watch', function() {
		
		/**
		 * Notify
		 */
		$.gutil.log('Gulp is running: ' + $.gutil.colors.black.bgYellow( 'dev' == BGVars.stage ? 'Development Mode' : 'Production Mode'));

		// if( BGVars.blindTest ) $.gutil.log('Blind Test Running : ' + $.gutil.colors.black.bgYellow( BGVars.blindTest ));
		/**
		 * Watches
		 */
		
		for (let index in BGTasks){
			$.gulp.watch( BGTasks[index].path, $.gulp.series( BGTasks[index].task ));
		}
	});
		
};