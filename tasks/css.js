'use strict';

module.exports = function ($, BGVars, BGPaths, folder) {
	$.gulp.task('css:' + folder, function(){

		var processors = [
			$.postcssSprites({
				imagePath      : BGPaths.src[folder].sprite,
				spritePath     : BGPaths.build[folder].sprite,
				stylesheetPath : BGPaths.build[folder].css
			}),
			$.autoprefixer({
				browsers: [
					'> 1%', 
					'Last 2 versions', 
					'IE 8'
				]
			}),
			$.mqpacker
		];

		if( 'prod'  == BGVars.stage ) {
			processors.push($.csswring({
				preserveHacks     : true,
				removeAllComments : true
			}));
		}
		
		return $.gulp.src( BGPaths.src[folder].css + '/**/*.scss')
			.pipe($.sourcemaps.init())
			.pipe($.sass.sync().on('error', $.sass.logError))
			.pipe($.postcss(processors))
				.on('error', $.functions.onError)
			.pipe($.sourcemaps.write('../maps'))
			.pipe($.gulp.dest( BGPaths.build[folder].css ))
			.pipe($.browserSync.stream({match: '**/*.css'}));
	});
};