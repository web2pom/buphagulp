'use strict';

module.exports = function ($, BGVars, BGPaths, folder) {
	$.gulp.task('files:' + folder, function(){
		return $.gulp.src(BGPaths.src[folder].files + '/**/*')
			.pipe($.changed(BGPaths.build[folder].files))
			.pipe($.gulp.dest(BGPaths.build[folder].files))
			.pipe($.browserSync.stream());
	});
};