'use strict';

module.exports = function ($, BGVars, BGPaths, folder) {
	$.gulp.task('html:' + folder, function(){

		return $.gulp.src(BGPaths.src[folder].html + '/**/*.html')
			.pipe($.data(function(file) {
				
				var data = {
					tpl :{
						root  : '',
						home  : './',
						stage : BGVars.stage,
						tag   : BGVars.tag ? '?v=' + BGVars.tag : false ,
						slug  : file.relative.replace(/\.html$/, "")
					}
				};

				for(let i = 1; i < file.relative.split('/').length; i++){
					data.tpl.root += '../';
				}

				if( '' !== data.tpl.root ) {
					data.home = data.tpl.root;
				}

				if( '' !== data.root ) {
					data.home = data.root;
				}

				return data;
			}))
			.pipe($.nunjucksRender({
				path: BGPaths.src[folder].html + '-templates'
			}))
				.on('error', $.functions.onError)
			.pipe($.gulpif('prod' == BGVars.stage, $.htmlmin({
				collapseWhitespace : true,
			})))
				.on('error', $.functions.onError)
			.pipe($.gulp.dest(BGPaths.build[folder].files))
			.pipe($.browserSync.stream());
	});
};