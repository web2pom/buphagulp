'use strict';

module.exports = function ($, BGVars, BGPaths, folder) {
	$.gulp.task('rsync:' + folder, function(){
		return $.gulp.src( BGPaths.build[folder].files + '/**' )
			.pipe($.rsync({
				root        : BGPaths.build[folder].files,
				hostname    : BGVars.build.hostname,
				destination : BGPaths.deploy[folder],
				incremental : true,
				progress    : true,
				recursive   : true,
				silent      : false,
				compress    : true
			}));
	});

	$.gulp.task('deploy:' + folder, $.gulp.series('build:' + folder, 'rsync:' + folder));
};