'use strict';

module.exports = function ($, BGVars, BGPaths, folder) {
	$.gulp.task('img:' + folder, function(){
		return $.gulp.src( BGPaths.src[folder].img + '/**/*')
			.pipe($.imagemin())
			.pipe($.gulp.dest( BGPaths.build[folder].img ))
			.pipe($.browserSync.stream());
	});
};