'use strict';

module.exports = function ($, BGVars, BGPaths, folder) {
	$.gulp.task('clean:' + folder, function(){
		return $.del( BGPaths.build[folder].files + '/**/*', {force:true}, function (err, paths) {
			console.log('Deleted files/folders:\n', paths.join('\n'));
		});
	});
};