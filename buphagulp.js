'use strict';

module.exports = function (project) {
	project = require('./helpers/defaults.js')(project);

	/** VARS */
	var $             = require('./parts/plugins.js'),
		BG_Vars       = require('./parts/vars.js')($, project),
		BG_Paths      = require('./parts/paths.js')(project, BG_Vars),
		requireTasks  = {
			watch    : [],
			clean    : [],
			build    : [],
			deploy   : [],
			byFolder : {},
			byTasks  : {
				'html'  : [], 
				'img'   : [], 
				'css'   : [], 
				'js'    : [], 
				'files' : []
			}
		};

	/** Cycle singles Tasks */
	for (let folder in project.folders) {
		requireTasks.byFolder[folder] = [];
		/** Build Task */
		for (let actualtask in requireTasks.byTasks){
			if( project.folders[folder].tasks[actualtask] ) {
				var taskname =  actualtask + ':' + folder;
				var taskpath = [ BG_Paths.src[folder][actualtask] + '/**/*' ];
				require('./tasks/' + actualtask + '.js')($, BG_Vars, BG_Paths, folder);

				if( 'html' == actualtask ) {
					taskpath.push(BG_Paths.src[folder][actualtask] + '-templates/**/*');
				}
				
				requireTasks.watch.push( {
					task : taskname,
					path : taskpath
				});

				requireTasks.byFolder[folder].push(taskname);
				requireTasks.byTasks[actualtask].push(taskname);
			}
		}

		/** Build */
		if (requireTasks.byFolder[folder].length > 0){
			$.gulp.task('build:' + folder, $.gulp.series(requireTasks.byFolder[folder]));
			requireTasks.build.push('build:' + folder);
		}

		/** Deploy */
		if( project.folders[folder].tasks.deploy ) {
			require('./tasks/deploy.js')($, BG_Vars, BG_Paths, folder);
			requireTasks.deploy.push('deploy:' + folder);
		}
			
		require('./tasks/clean.js')($, BG_Vars, BG_Paths, folder);
		requireTasks.clean.push('clean:' + folder);
	}

	/** Main Tasks */
	for (let actualtask in requireTasks.byTasks){
		$.gulp.task(actualtask, $.gulp.series(requireTasks.byTasks[actualtask]));
	}

	/** Clean */
	$.gulp.task('clean', $.gulp.series(requireTasks.clean));

	/** Build */
	$.gulp.task('build', $.gulp.series(requireTasks.build));

	/** Deploy */
	$.gulp.task('deploy', $.gulp.series(requireTasks.deploy));
	
	/** Watch Tasks */
	require('./tasks/watch.js')($, BG_Vars, BG_Paths, requireTasks.watch, project);

	/** Default Tasks */
	$.gulp.task(
		'default',
		$.gulp.series('build', $.gulp.parallel('browser-sync', 'watch'))
	);

	/** DEBUG Tasks */
	$.gulp.task('debug', function() {
		console.log($);
	});
};