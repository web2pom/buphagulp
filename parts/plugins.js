'use strict';

/***************************************************************************************************
 ****************************************************************************************************
 * Plugins
 ****************************************************************************************************
 ****************************************************************************************************/

var plugins = {
	gulp            : require('gulp'),
	util            : require('util'),
	gutil           : require('gulp-util'),
	gulpif          : require('gulp-if'),
	imagemin        : require('gulp-imagemin'),
	changed         : require('gulp-changed'),
	rsync           : require('gulp-rsync'),
	del             : require('del'),
	browserSync     : require('browser-sync'),
	/** HTML **/
	nunjucksRender  : require('gulp-nunjucks-render'),
	data            : require('gulp-data'),
	htmlmin         : require('gulp-htmlmin'),
	/** JS **/
	jshint          : require('gulp-jshint'),
	uglify          : require('gulp-uglify'),
	// concat          : require('gulp-concat'),
	// babel           : require('gulp-babel'),
	include         : require('gulp-include'),
	/** CSS **/
	sourcemaps      : require('gulp-sourcemaps'),
	sass            : require('gulp-sass'),
	postcss         : require('gulp-postcss'),
	postcssSprites  : require('postcss-easysprites'),
	autoprefixer    : require('autoprefixer'),
	mqpacker        : require('css-mqpacker'),
	csswring        : require('csswring'),
	/* WP LOCALIZE i18n */
	// wpPot           : require('gulp-wp-pot'),
	// sort            : require('gulp-sort'),
};

/* Functions */
plugins.functions = {
	onError         : function(e){
						plugins.gutil.log(e);
						this.end();
					}
};


module.exports = plugins;