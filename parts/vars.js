'use strict';

/****************************************************************************************************
 ****************************************************************************************************
 * Config globVars
 ****************************************************************************************************
 ****************************************************************************************************/
module.exports = function ($,project) {

	var BGVars = {};

	BGVars.stage  = $.gutil.env.stage  === 'prod'  ? 'prod'  : 'dev';
	BGVars.tag    = project.tag;
	BGVars.build  = project.builds[BGVars.stage];

	if( false === BGVars.build || false === BGVars.build.path ) {
		BGVars.build.path = './builds/' + BGVars.stage;
	}

	/*if( $.gutil.env.blindTest ){

		var blindTestArray = [			
			'protanomaly',
			'protanopia',
			'deuteranomaly',
			'deuteranopia',
			'tritanomaly',
			'tritanopia',
			'achromatomaly',
			'achromatopsia'
		];

		var methodnumber   = 'number' === typeof $.gutil.env.blindTest ? $.gutil.env.blindTest : 0;
		var methodnumber   = methodnumber >= blindTestArray.length   ? methodnumber % 8 : methodnumber;

		BGVars.blindTest = blindTestArray[methodnumber];

	} else BGVars.blindTest = false;*/

	return BGVars;
};