'use strict';
/****************************************************************************************************
 ****************************************************************************************************
 * Config BGPaths
 ****************************************************************************************************
 ****************************************************************************************************/
module.exports = function (project,BGVars) {

	var BGPaths = {
		src    : {},
		build  : {},
		deploy : {}
	};

	for (let folder in project.folders) {
		BGPaths.src[folder] = {
				files     : './src/' + folder + '/files',
				html      : './src/' + folder + '/html',
				css       : './src/' + folder + '/css',
				js        : './src/' + folder + '/js',
				img       : './src/' + folder + '/img',
				sprite    : './src/' + folder + '/sprite'
		};

		let subpath = false;

		if( project.folders[folder].build ){
			subpath            = project.folders[folder].build;
			BGPaths.deploy[folder] = BGVars.build.remotePath + project.folders[folder].build;
		} else {
			subpath            = '/' + folder;
			BGPaths.deploy[folder] = BGVars.build.remotePath;
		}


		BGPaths.build[folder] = {
				files  : BGVars.build.path + subpath,
				css    : BGVars.build.path + subpath + '/css',
				js     : BGVars.build.path + subpath + '/js',
				img    : BGVars.build.path + subpath + '/img',
				sprite : BGVars.build.path + subpath + '/sprites'
		};
		
	}

	return BGPaths;
};