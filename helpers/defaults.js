'use strict';

module.exports = function (project) {

	var deepExtend = function(out) {
		out = out || {};

		for (var i = 1; i < arguments.length; i++) {
			var obj = arguments[i];

			if (!obj){
				continue;
			}

			for (var key in obj) {
				if (obj.hasOwnProperty(key)) {
					if (typeof obj[key] === 'object'){
						out[key] = deepExtend(out[key], obj[key]);
					} else{
						out[key] = obj[key];
					}
				}
			}
		}

		return out;
	};

	project = deepExtend( {}, {
	 	"local"     : {
			"proxy" : false,
			"host"  : "127.0.0.1"
		},
		"builds"    : {
			"dev"   : {
				"path"       : false,
				"hostname"   : false,
				"remotePath" : false
			},
			"prod"  : {
				"path"       : false,
				"hostname"   : false,
				"remotePath" : false 
			}
		},
		"folders"   : {},
		"i18n"      : false,
		"tag"       : false
	}, project);

	for (let key in project.folders){
		project.folders[key] = deepExtend({
			"build"  : false,
			"tasks"  : {
				"img"    : false,
				"css"    : false,
				"js"     : false,
				"files"  : false,
				"html"   : false,
				"deploy" : false
			}
		}, project.folders[key]);
	}

	for (let key in project.builds){
		project.builds[key] = deepExtend({
			"path"       : false,
			"hostname"   : false,
			"remotePath" : false
		}, project.builds[key]);
	}

	return project;
};