# buphagulp #

## Installazione

	yarn init && yarn add gitlab:web2pom/buphagulp

## Creare le cartelle
Per creare le cartelle di default per un progetto wordpress basta dare

	mkdir -p ./src/{themes,muplugins}/{img,css,js,files,sprite}
	
Per un progetto statico

	mkdir -p ./src/static/{img,css,js,files,sprite}


## Esempio gulpfile.js

```
buphagulp = require('buphagulp');


buphagulp({
 	"local"     : {
		"proxy" : "http://127.0.0.1",
		"host"  : "127.0.0.1"	
 	},
	"builds"    : {
		"dev"   : {
			"path"       : "/local/path/to/wp-content",
			"hostname"   : "server-address",
			"remotePath" : "/remote/path/to/wp-content"
		},
		"prod"  : {
			"path"       : false,
			"hostname"   : false,
			"remotePath" : false 
		}
	},
	"folders"      : {
		"theme" : {
			"build"  : "/themes/theme-name",
			"tasks"  : {
				"img"    : true,
				"css"    : true,
				"js"     : true,
				"files"  : true,
				"deploy" : true
			}
		},
		"muplugins" : {
			"build"  : "/mu-plugins",
			"tasks"  : {
				"img"    : false,
				"css"    : false,
				"js"     : false,
				"files"  : true,
				"deploy" : true
			}
		}
	},
	"i18n"       : false
});
```